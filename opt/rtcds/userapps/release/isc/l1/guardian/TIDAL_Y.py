import TIDAL
TIDAL.ARM = 'Y'
TIDAL.ISIGAIN = -0.4
TIDAL.HPIGAIN = 0.4
TIDAL.HPIGAIN_CTIDAL = -1 #JCB, since TIDAL_M0 already has a gain of 2, changing this to -1, used to be -2. 20191126
TIDAL.HPIGAIN_DTIDAL = 1 #JCB, since M0_LOCK_GAIN is 0.4, setting this to be 1, used to be 0.4.
from TIDAL import *
nominal = 'HPI_SERVO_ON_CTIDAL'
